// Code generated by protoc-gen-go. DO NOT EDIT.
// source: yggdrasil/yggdrasil.proto

package yggdrasil

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

//Apricot
type Access struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Rpc                  string   `protobuf:"bytes,2,opt,name=rpc,proto3" json:"rpc,omitempty"`
	Slug                 string   `protobuf:"bytes,3,opt,name=slug,proto3" json:"slug,omitempty"`
	Service              string   `protobuf:"bytes,4,opt,name=service,proto3" json:"service,omitempty"`
	CreatedAt            string   `protobuf:"bytes,5,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,6,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Access) Reset()         { *m = Access{} }
func (m *Access) String() string { return proto.CompactTextString(m) }
func (*Access) ProtoMessage()    {}
func (*Access) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{0}
}

func (m *Access) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Access.Unmarshal(m, b)
}
func (m *Access) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Access.Marshal(b, m, deterministic)
}
func (m *Access) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Access.Merge(m, src)
}
func (m *Access) XXX_Size() int {
	return xxx_messageInfo_Access.Size(m)
}
func (m *Access) XXX_DiscardUnknown() {
	xxx_messageInfo_Access.DiscardUnknown(m)
}

var xxx_messageInfo_Access proto.InternalMessageInfo

func (m *Access) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Access) GetRpc() string {
	if m != nil {
		return m.Rpc
	}
	return ""
}

func (m *Access) GetSlug() string {
	if m != nil {
		return m.Slug
	}
	return ""
}

func (m *Access) GetService() string {
	if m != nil {
		return m.Service
	}
	return ""
}

func (m *Access) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *Access) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type Auth struct {
	Expires              int64    `protobuf:"varint,1,opt,name=expires,proto3" json:"expires,omitempty"`
	RefreshToken         string   `protobuf:"bytes,2,opt,name=refreshToken,proto3" json:"refreshToken,omitempty"`
	Token                string   `protobuf:"bytes,3,opt,name=token,proto3" json:"token,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Auth) Reset()         { *m = Auth{} }
func (m *Auth) String() string { return proto.CompactTextString(m) }
func (*Auth) ProtoMessage()    {}
func (*Auth) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{1}
}

func (m *Auth) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Auth.Unmarshal(m, b)
}
func (m *Auth) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Auth.Marshal(b, m, deterministic)
}
func (m *Auth) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Auth.Merge(m, src)
}
func (m *Auth) XXX_Size() int {
	return xxx_messageInfo_Auth.Size(m)
}
func (m *Auth) XXX_DiscardUnknown() {
	xxx_messageInfo_Auth.DiscardUnknown(m)
}

var xxx_messageInfo_Auth proto.InternalMessageInfo

func (m *Auth) GetExpires() int64 {
	if m != nil {
		return m.Expires
	}
	return 0
}

func (m *Auth) GetRefreshToken() string {
	if m != nil {
		return m.RefreshToken
	}
	return ""
}

func (m *Auth) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

type Client struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	StudioId             string   `protobuf:"bytes,2,opt,name=studioId,proto3" json:"studioId,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	Slug                 string   `protobuf:"bytes,4,opt,name=slug,proto3" json:"slug,omitempty"`
	ClientId             string   `protobuf:"bytes,5,opt,name=clientId,proto3" json:"clientId,omitempty"`
	ClientSecret         string   `protobuf:"bytes,6,opt,name=clientSecret,proto3" json:"clientSecret,omitempty"`
	CreatedAt            string   `protobuf:"bytes,7,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,8,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Client) Reset()         { *m = Client{} }
func (m *Client) String() string { return proto.CompactTextString(m) }
func (*Client) ProtoMessage()    {}
func (*Client) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{2}
}

func (m *Client) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Client.Unmarshal(m, b)
}
func (m *Client) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Client.Marshal(b, m, deterministic)
}
func (m *Client) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Client.Merge(m, src)
}
func (m *Client) XXX_Size() int {
	return xxx_messageInfo_Client.Size(m)
}
func (m *Client) XXX_DiscardUnknown() {
	xxx_messageInfo_Client.DiscardUnknown(m)
}

var xxx_messageInfo_Client proto.InternalMessageInfo

func (m *Client) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Client) GetStudioId() string {
	if m != nil {
		return m.StudioId
	}
	return ""
}

func (m *Client) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Client) GetSlug() string {
	if m != nil {
		return m.Slug
	}
	return ""
}

func (m *Client) GetClientId() string {
	if m != nil {
		return m.ClientId
	}
	return ""
}

func (m *Client) GetClientSecret() string {
	if m != nil {
		return m.ClientSecret
	}
	return ""
}

func (m *Client) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *Client) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type ClientStudio struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Studio               *Studio  `protobuf:"bytes,2,opt,name=studio,proto3" json:"studio,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	Slug                 string   `protobuf:"bytes,4,opt,name=slug,proto3" json:"slug,omitempty"`
	ClientId             string   `protobuf:"bytes,5,opt,name=clientId,proto3" json:"clientId,omitempty"`
	ClientSecret         string   `protobuf:"bytes,6,opt,name=clientSecret,proto3" json:"clientSecret,omitempty"`
	CreatedAt            string   `protobuf:"bytes,7,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,8,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ClientStudio) Reset()         { *m = ClientStudio{} }
func (m *ClientStudio) String() string { return proto.CompactTextString(m) }
func (*ClientStudio) ProtoMessage()    {}
func (*ClientStudio) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{3}
}

func (m *ClientStudio) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ClientStudio.Unmarshal(m, b)
}
func (m *ClientStudio) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ClientStudio.Marshal(b, m, deterministic)
}
func (m *ClientStudio) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ClientStudio.Merge(m, src)
}
func (m *ClientStudio) XXX_Size() int {
	return xxx_messageInfo_ClientStudio.Size(m)
}
func (m *ClientStudio) XXX_DiscardUnknown() {
	xxx_messageInfo_ClientStudio.DiscardUnknown(m)
}

var xxx_messageInfo_ClientStudio proto.InternalMessageInfo

func (m *ClientStudio) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *ClientStudio) GetStudio() *Studio {
	if m != nil {
		return m.Studio
	}
	return nil
}

func (m *ClientStudio) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *ClientStudio) GetSlug() string {
	if m != nil {
		return m.Slug
	}
	return ""
}

func (m *ClientStudio) GetClientId() string {
	if m != nil {
		return m.ClientId
	}
	return ""
}

func (m *ClientStudio) GetClientSecret() string {
	if m != nil {
		return m.ClientSecret
	}
	return ""
}

func (m *ClientStudio) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *ClientStudio) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type ScopeAccess struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Scope                *Scope   `protobuf:"bytes,2,opt,name=scope,proto3" json:"scope,omitempty"`
	Access               *Access  `protobuf:"bytes,3,opt,name=access,proto3" json:"access,omitempty"`
	CreatedAt            string   `protobuf:"bytes,4,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,5,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ScopeAccess) Reset()         { *m = ScopeAccess{} }
func (m *ScopeAccess) String() string { return proto.CompactTextString(m) }
func (*ScopeAccess) ProtoMessage()    {}
func (*ScopeAccess) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{4}
}

func (m *ScopeAccess) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ScopeAccess.Unmarshal(m, b)
}
func (m *ScopeAccess) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ScopeAccess.Marshal(b, m, deterministic)
}
func (m *ScopeAccess) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ScopeAccess.Merge(m, src)
}
func (m *ScopeAccess) XXX_Size() int {
	return xxx_messageInfo_ScopeAccess.Size(m)
}
func (m *ScopeAccess) XXX_DiscardUnknown() {
	xxx_messageInfo_ScopeAccess.DiscardUnknown(m)
}

var xxx_messageInfo_ScopeAccess proto.InternalMessageInfo

func (m *ScopeAccess) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *ScopeAccess) GetScope() *Scope {
	if m != nil {
		return m.Scope
	}
	return nil
}

func (m *ScopeAccess) GetAccess() *Access {
	if m != nil {
		return m.Access
	}
	return nil
}

func (m *ScopeAccess) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *ScopeAccess) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type Scope struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Slug                 string   `protobuf:"bytes,3,opt,name=slug,proto3" json:"slug,omitempty"`
	CreatedAt            string   `protobuf:"bytes,4,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,5,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Scope) Reset()         { *m = Scope{} }
func (m *Scope) String() string { return proto.CompactTextString(m) }
func (*Scope) ProtoMessage()    {}
func (*Scope) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{5}
}

func (m *Scope) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Scope.Unmarshal(m, b)
}
func (m *Scope) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Scope.Marshal(b, m, deterministic)
}
func (m *Scope) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Scope.Merge(m, src)
}
func (m *Scope) XXX_Size() int {
	return xxx_messageInfo_Scope.Size(m)
}
func (m *Scope) XXX_DiscardUnknown() {
	xxx_messageInfo_Scope.DiscardUnknown(m)
}

var xxx_messageInfo_Scope proto.InternalMessageInfo

func (m *Scope) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Scope) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Scope) GetSlug() string {
	if m != nil {
		return m.Slug
	}
	return ""
}

func (m *Scope) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *Scope) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type Studio struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Slug                 string   `protobuf:"bytes,3,opt,name=slug,proto3" json:"slug,omitempty"`
	CreatedAt            string   `protobuf:"bytes,4,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,5,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Studio) Reset()         { *m = Studio{} }
func (m *Studio) String() string { return proto.CompactTextString(m) }
func (*Studio) ProtoMessage()    {}
func (*Studio) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{6}
}

func (m *Studio) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Studio.Unmarshal(m, b)
}
func (m *Studio) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Studio.Marshal(b, m, deterministic)
}
func (m *Studio) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Studio.Merge(m, src)
}
func (m *Studio) XXX_Size() int {
	return xxx_messageInfo_Studio.Size(m)
}
func (m *Studio) XXX_DiscardUnknown() {
	xxx_messageInfo_Studio.DiscardUnknown(m)
}

var xxx_messageInfo_Studio proto.InternalMessageInfo

func (m *Studio) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Studio) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Studio) GetSlug() string {
	if m != nil {
		return m.Slug
	}
	return ""
}

func (m *Studio) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *Studio) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type UserProvider struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Slug                 string   `protobuf:"bytes,3,opt,name=slug,proto3" json:"slug,omitempty"`
	CreatedAt            string   `protobuf:"bytes,4,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,5,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserProvider) Reset()         { *m = UserProvider{} }
func (m *UserProvider) String() string { return proto.CompactTextString(m) }
func (*UserProvider) ProtoMessage()    {}
func (*UserProvider) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{7}
}

func (m *UserProvider) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserProvider.Unmarshal(m, b)
}
func (m *UserProvider) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserProvider.Marshal(b, m, deterministic)
}
func (m *UserProvider) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserProvider.Merge(m, src)
}
func (m *UserProvider) XXX_Size() int {
	return xxx_messageInfo_UserProvider.Size(m)
}
func (m *UserProvider) XXX_DiscardUnknown() {
	xxx_messageInfo_UserProvider.DiscardUnknown(m)
}

var xxx_messageInfo_UserProvider proto.InternalMessageInfo

func (m *UserProvider) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *UserProvider) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *UserProvider) GetSlug() string {
	if m != nil {
		return m.Slug
	}
	return ""
}

func (m *UserProvider) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *UserProvider) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type UserScope struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	User                 *User    `protobuf:"bytes,2,opt,name=user,proto3" json:"user,omitempty"`
	Scope                *Scope   `protobuf:"bytes,3,opt,name=scope,proto3" json:"scope,omitempty"`
	CreatedAt            string   `protobuf:"bytes,4,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,5,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserScope) Reset()         { *m = UserScope{} }
func (m *UserScope) String() string { return proto.CompactTextString(m) }
func (*UserScope) ProtoMessage()    {}
func (*UserScope) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{8}
}

func (m *UserScope) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserScope.Unmarshal(m, b)
}
func (m *UserScope) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserScope.Marshal(b, m, deterministic)
}
func (m *UserScope) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserScope.Merge(m, src)
}
func (m *UserScope) XXX_Size() int {
	return xxx_messageInfo_UserScope.Size(m)
}
func (m *UserScope) XXX_DiscardUnknown() {
	xxx_messageInfo_UserScope.DiscardUnknown(m)
}

var xxx_messageInfo_UserScope proto.InternalMessageInfo

func (m *UserScope) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *UserScope) GetUser() *User {
	if m != nil {
		return m.User
	}
	return nil
}

func (m *UserScope) GetScope() *Scope {
	if m != nil {
		return m.Scope
	}
	return nil
}

func (m *UserScope) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *UserScope) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type User struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	DateOfBirth          int64    `protobuf:"varint,2,opt,name=dateOfBirth,proto3" json:"dateOfBirth,omitempty"`
	Email                string   `protobuf:"bytes,3,opt,name=email,proto3" json:"email,omitempty"`
	Fullname             string   `protobuf:"bytes,4,opt,name=fullname,proto3" json:"fullname,omitempty"`
	Password             string   `protobuf:"bytes,5,opt,name=password,proto3" json:"password,omitempty"`
	PhoneNumber          string   `protobuf:"bytes,6,opt,name=phoneNumber,proto3" json:"phoneNumber,omitempty"`
	ProfilePic           string   `protobuf:"bytes,7,opt,name=profilePic,proto3" json:"profilePic,omitempty"`
	CreatedAt            string   `protobuf:"bytes,8,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,9,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *User) Reset()         { *m = User{} }
func (m *User) String() string { return proto.CompactTextString(m) }
func (*User) ProtoMessage()    {}
func (*User) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{9}
}

func (m *User) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_User.Unmarshal(m, b)
}
func (m *User) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_User.Marshal(b, m, deterministic)
}
func (m *User) XXX_Merge(src proto.Message) {
	xxx_messageInfo_User.Merge(m, src)
}
func (m *User) XXX_Size() int {
	return xxx_messageInfo_User.Size(m)
}
func (m *User) XXX_DiscardUnknown() {
	xxx_messageInfo_User.DiscardUnknown(m)
}

var xxx_messageInfo_User proto.InternalMessageInfo

func (m *User) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *User) GetDateOfBirth() int64 {
	if m != nil {
		return m.DateOfBirth
	}
	return 0
}

func (m *User) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *User) GetFullname() string {
	if m != nil {
		return m.Fullname
	}
	return ""
}

func (m *User) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

func (m *User) GetPhoneNumber() string {
	if m != nil {
		return m.PhoneNumber
	}
	return ""
}

func (m *User) GetProfilePic() string {
	if m != nil {
		return m.ProfilePic
	}
	return ""
}

func (m *User) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *User) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

// Argan
type CompanyCustomer struct {
	Id                   string    `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Company              *Company  `protobuf:"bytes,2,opt,name=company,proto3" json:"company,omitempty"`
	Customer             *Customer `protobuf:"bytes,3,opt,name=customer,proto3" json:"customer,omitempty"`
	CreatedAt            string    `protobuf:"bytes,4,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string    `protobuf:"bytes,5,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *CompanyCustomer) Reset()         { *m = CompanyCustomer{} }
func (m *CompanyCustomer) String() string { return proto.CompactTextString(m) }
func (*CompanyCustomer) ProtoMessage()    {}
func (*CompanyCustomer) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{10}
}

func (m *CompanyCustomer) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CompanyCustomer.Unmarshal(m, b)
}
func (m *CompanyCustomer) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CompanyCustomer.Marshal(b, m, deterministic)
}
func (m *CompanyCustomer) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CompanyCustomer.Merge(m, src)
}
func (m *CompanyCustomer) XXX_Size() int {
	return xxx_messageInfo_CompanyCustomer.Size(m)
}
func (m *CompanyCustomer) XXX_DiscardUnknown() {
	xxx_messageInfo_CompanyCustomer.DiscardUnknown(m)
}

var xxx_messageInfo_CompanyCustomer proto.InternalMessageInfo

func (m *CompanyCustomer) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *CompanyCustomer) GetCompany() *Company {
	if m != nil {
		return m.Company
	}
	return nil
}

func (m *CompanyCustomer) GetCustomer() *Customer {
	if m != nil {
		return m.Customer
	}
	return nil
}

func (m *CompanyCustomer) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *CompanyCustomer) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type Company struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Slug                 string   `protobuf:"bytes,3,opt,name=slug,proto3" json:"slug,omitempty"`
	ShortName            string   `protobuf:"bytes,4,opt,name=shortName,proto3" json:"shortName,omitempty"`
	Address              string   `protobuf:"bytes,5,opt,name=address,proto3" json:"address,omitempty"`
	Code                 string   `protobuf:"bytes,6,opt,name=code,proto3" json:"code,omitempty"`
	Client               *Client  `protobuf:"bytes,7,opt,name=client,proto3" json:"client,omitempty"`
	CreatedAt            string   `protobuf:"bytes,8,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,9,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Company) Reset()         { *m = Company{} }
func (m *Company) String() string { return proto.CompactTextString(m) }
func (*Company) ProtoMessage()    {}
func (*Company) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{11}
}

func (m *Company) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Company.Unmarshal(m, b)
}
func (m *Company) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Company.Marshal(b, m, deterministic)
}
func (m *Company) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Company.Merge(m, src)
}
func (m *Company) XXX_Size() int {
	return xxx_messageInfo_Company.Size(m)
}
func (m *Company) XXX_DiscardUnknown() {
	xxx_messageInfo_Company.DiscardUnknown(m)
}

var xxx_messageInfo_Company proto.InternalMessageInfo

func (m *Company) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Company) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Company) GetSlug() string {
	if m != nil {
		return m.Slug
	}
	return ""
}

func (m *Company) GetShortName() string {
	if m != nil {
		return m.ShortName
	}
	return ""
}

func (m *Company) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

func (m *Company) GetCode() string {
	if m != nil {
		return m.Code
	}
	return ""
}

func (m *Company) GetClient() *Client {
	if m != nil {
		return m.Client
	}
	return nil
}

func (m *Company) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *Company) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type Customer struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	User                 *User    `protobuf:"bytes,2,opt,name=user,proto3" json:"user,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	KtpNo                string   `protobuf:"bytes,4,opt,name=ktpNo,proto3" json:"ktpNo,omitempty"`
	KtpAsset             string   `protobuf:"bytes,5,opt,name=ktpAsset,proto3" json:"ktpAsset,omitempty"`
	BirthPlace           string   `protobuf:"bytes,6,opt,name=birthPlace,proto3" json:"birthPlace,omitempty"`
	BirthDate            int64    `protobuf:"varint,7,opt,name=birthDate,proto3" json:"birthDate,omitempty"`
	Address              string   `protobuf:"bytes,8,opt,name=address,proto3" json:"address,omitempty"`
	Gender               string   `protobuf:"bytes,9,opt,name=gender,proto3" json:"gender,omitempty"`
	CreatedAt            string   `protobuf:"bytes,10,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,11,opt,name=updatedAt,proto3" json:"updatedAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Customer) Reset()         { *m = Customer{} }
func (m *Customer) String() string { return proto.CompactTextString(m) }
func (*Customer) ProtoMessage()    {}
func (*Customer) Descriptor() ([]byte, []int) {
	return fileDescriptor_3220c51febdf2137, []int{12}
}

func (m *Customer) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Customer.Unmarshal(m, b)
}
func (m *Customer) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Customer.Marshal(b, m, deterministic)
}
func (m *Customer) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Customer.Merge(m, src)
}
func (m *Customer) XXX_Size() int {
	return xxx_messageInfo_Customer.Size(m)
}
func (m *Customer) XXX_DiscardUnknown() {
	xxx_messageInfo_Customer.DiscardUnknown(m)
}

var xxx_messageInfo_Customer proto.InternalMessageInfo

func (m *Customer) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Customer) GetUser() *User {
	if m != nil {
		return m.User
	}
	return nil
}

func (m *Customer) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Customer) GetKtpNo() string {
	if m != nil {
		return m.KtpNo
	}
	return ""
}

func (m *Customer) GetKtpAsset() string {
	if m != nil {
		return m.KtpAsset
	}
	return ""
}

func (m *Customer) GetBirthPlace() string {
	if m != nil {
		return m.BirthPlace
	}
	return ""
}

func (m *Customer) GetBirthDate() int64 {
	if m != nil {
		return m.BirthDate
	}
	return 0
}

func (m *Customer) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

func (m *Customer) GetGender() string {
	if m != nil {
		return m.Gender
	}
	return ""
}

func (m *Customer) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *Customer) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

func init() {
	proto.RegisterType((*Access)(nil), "yggdrasil.Access")
	proto.RegisterType((*Auth)(nil), "yggdrasil.Auth")
	proto.RegisterType((*Client)(nil), "yggdrasil.Client")
	proto.RegisterType((*ClientStudio)(nil), "yggdrasil.ClientStudio")
	proto.RegisterType((*ScopeAccess)(nil), "yggdrasil.ScopeAccess")
	proto.RegisterType((*Scope)(nil), "yggdrasil.Scope")
	proto.RegisterType((*Studio)(nil), "yggdrasil.Studio")
	proto.RegisterType((*UserProvider)(nil), "yggdrasil.UserProvider")
	proto.RegisterType((*UserScope)(nil), "yggdrasil.UserScope")
	proto.RegisterType((*User)(nil), "yggdrasil.User")
	proto.RegisterType((*CompanyCustomer)(nil), "yggdrasil.CompanyCustomer")
	proto.RegisterType((*Company)(nil), "yggdrasil.Company")
	proto.RegisterType((*Customer)(nil), "yggdrasil.Customer")
}

func init() { proto.RegisterFile("yggdrasil/yggdrasil.proto", fileDescriptor_3220c51febdf2137) }

var fileDescriptor_3220c51febdf2137 = []byte{
	// 719 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xd4, 0x56, 0xc1, 0x6e, 0xd3, 0x4a,
	0x14, 0x95, 0x13, 0xc7, 0x71, 0x6e, 0xaa, 0xd7, 0x3e, 0xbf, 0xea, 0x29, 0xaf, 0x7a, 0x42, 0x55,
	0x90, 0x10, 0x48, 0xa8, 0x95, 0xe0, 0x0b, 0x42, 0xd9, 0x74, 0x53, 0x2a, 0x17, 0x36, 0xec, 0xdc,
	0x99, 0x9b, 0xc4, 0xaa, 0x93, 0xb1, 0x66, 0xc6, 0x85, 0x0a, 0x56, 0x88, 0x4f, 0xe0, 0x13, 0x58,
	0xb2, 0x61, 0xcf, 0x87, 0xf0, 0x13, 0xec, 0xf8, 0x00, 0x74, 0x67, 0xc6, 0x8e, 0x9d, 0xa4, 0x11,
	0x52, 0x24, 0x10, 0xbb, 0xb9, 0xe7, 0xde, 0xcc, 0x9c, 0x73, 0xee, 0x9d, 0x71, 0xe0, 0xbf, 0x9b,
	0xc9, 0x84, 0xcb, 0x44, 0xa5, 0xd9, 0x71, 0xb5, 0x3a, 0xca, 0xa5, 0xd0, 0x22, 0xea, 0x55, 0xc0,
	0xf0, 0x83, 0x07, 0xc1, 0x88, 0x31, 0x54, 0x2a, 0xfa, 0x0b, 0x5a, 0x29, 0x1f, 0x78, 0x87, 0xde,
	0xfd, 0x5e, 0xdc, 0x4a, 0x79, 0xb4, 0x07, 0x6d, 0x99, 0xb3, 0x41, 0xcb, 0x00, 0xb4, 0x8c, 0x22,
	0xf0, 0x55, 0x56, 0x4c, 0x06, 0x6d, 0x03, 0x99, 0x75, 0x34, 0x80, 0xae, 0x42, 0x79, 0x9d, 0x32,
	0x1c, 0xf8, 0x06, 0x2e, 0xc3, 0xe8, 0x7f, 0xe8, 0x31, 0x89, 0x89, 0x46, 0x3e, 0xd2, 0x83, 0x8e,
	0xc9, 0x2d, 0x00, 0xca, 0x16, 0x39, 0x77, 0xd9, 0xc0, 0x66, 0x2b, 0x60, 0xf8, 0x12, 0xfc, 0x51,
	0xa1, 0xa7, 0xb4, 0x3b, 0xbe, 0xce, 0x53, 0x89, 0xca, 0x10, 0x6b, 0xc7, 0x65, 0x18, 0x0d, 0x61,
	0x47, 0xe2, 0x58, 0xa2, 0x9a, 0x3e, 0x17, 0x57, 0x38, 0x77, 0x34, 0x1b, 0x58, 0xb4, 0x0f, 0x1d,
	0x6d, 0x92, 0x96, 0xb0, 0x0d, 0x86, 0x5f, 0x3d, 0x08, 0x4e, 0xb2, 0x14, 0xe7, 0x7a, 0x45, 0xf2,
	0x01, 0x84, 0x4a, 0x17, 0x3c, 0x15, 0xa7, 0xdc, 0x6d, 0x58, 0xc5, 0x24, 0x7e, 0x9e, 0xcc, 0xb0,
	0x14, 0x4f, 0xeb, 0xca, 0x10, 0xbf, 0x66, 0xc8, 0x01, 0x84, 0xcc, 0xec, 0x7e, 0xca, 0x9d, 0xea,
	0x2a, 0x26, 0xd2, 0x76, 0x7d, 0x81, 0x4c, 0x62, 0xa9, 0xbb, 0x81, 0x35, 0x6d, 0xeb, 0x6e, 0xb4,
	0x2d, 0x5c, 0xb6, 0xed, 0x9b, 0x07, 0x3b, 0x56, 0xda, 0x85, 0xa1, 0xbd, 0x22, 0xf0, 0x01, 0x04,
	0x56, 0x90, 0x91, 0xd7, 0x7f, 0xf4, 0xf7, 0xd1, 0x62, 0x36, 0xec, 0x4f, 0x62, 0x57, 0xf0, 0x47,
	0xe8, 0xfd, 0xe4, 0x41, 0xff, 0x82, 0x89, 0x1c, 0x6f, 0x19, 0xe1, 0x7b, 0xd0, 0x51, 0x94, 0x76,
	0x6a, 0xf7, 0xea, 0x6a, 0x09, 0x8f, 0x6d, 0x9a, 0x6c, 0x49, 0xcc, 0x0e, 0x46, 0x6d, 0xd3, 0x16,
	0xbb, 0x75, 0xec, 0x0a, 0x9a, 0x74, 0xfd, 0x8d, 0x74, 0x3b, 0xcb, 0x74, 0xdf, 0x40, 0xc7, 0x1c,
	0xbb, 0xc2, 0xb3, 0xf4, 0xba, 0xb5, 0xc6, 0xeb, 0xfa, 0x65, 0xdb, 0xe6, 0xf0, 0xb7, 0x10, 0xdc,
	0x32, 0x14, 0xbf, 0xe2, 0xf4, 0x77, 0x1e, 0xec, 0xbc, 0x50, 0x28, 0xcf, 0xa5, 0xb8, 0x4e, 0x39,
	0xca, 0xdf, 0x42, 0xe2, 0xa3, 0x07, 0x3d, 0x22, 0xb1, 0xbe, 0x09, 0x77, 0xc1, 0x2f, 0x14, 0x4a,
	0x37, 0x2b, 0xbb, 0xb5, 0x11, 0xa0, 0xdf, 0xc4, 0x26, 0xb9, 0x98, 0xa8, 0xf6, 0xe6, 0x89, 0xda,
	0x86, 0xe6, 0xfb, 0x16, 0xf8, 0x74, 0xe4, 0x0a, 0xc3, 0x43, 0xe8, 0x53, 0xcd, 0xb3, 0xf1, 0x93,
	0x54, 0xea, 0xa9, 0x21, 0xda, 0x8e, 0xeb, 0x10, 0xbd, 0x78, 0x38, 0x4b, 0xd2, 0xac, 0x7c, 0xf1,
	0x4c, 0x40, 0x57, 0x74, 0x5c, 0x64, 0x99, 0xf1, 0xd7, 0x72, 0xa9, 0x62, 0xca, 0xe5, 0x89, 0x52,
	0xaf, 0x84, 0xac, 0xae, 0x6f, 0x19, 0xd3, 0x79, 0xf9, 0x54, 0xcc, 0xf1, 0xac, 0x98, 0x5d, 0xa2,
	0x74, 0xb7, 0xb7, 0x0e, 0x45, 0x77, 0x00, 0x72, 0x29, 0xc6, 0x69, 0x86, 0xe7, 0x29, 0x73, 0xb7,
	0xb7, 0x86, 0x34, 0x6d, 0x08, 0x37, 0xda, 0xd0, 0x5b, 0xb6, 0xe1, 0x8b, 0x07, 0xbb, 0x27, 0x62,
	0x96, 0x27, 0xf3, 0x9b, 0x93, 0x42, 0x69, 0x31, 0x5b, 0xe3, 0xc8, 0x43, 0xe8, 0x32, 0x5b, 0xe2,
	0xda, 0x16, 0xd5, 0x1a, 0xe2, 0x7e, 0x1c, 0x97, 0x25, 0xd1, 0x31, 0x84, 0xcc, 0xed, 0xe4, 0xfa,
	0xf7, 0x4f, 0xbd, 0xdc, 0xa5, 0xe2, 0xaa, 0x68, 0xab, 0x2e, 0x7e, 0xf7, 0xa0, 0xeb, 0x18, 0x6c,
	0x33, 0xec, 0x6a, 0x2a, 0xa4, 0x3e, 0x5b, 0x74, 0x6e, 0x01, 0xd0, 0xc7, 0x31, 0xe1, 0x5c, 0xd2,
	0xb3, 0x65, 0x4f, 0x2f, 0x43, 0xda, 0x8b, 0x09, 0x8e, 0xae, 0x63, 0x66, 0x4d, 0x6f, 0x9c, 0x7d,
	0x77, 0x4d, 0x9b, 0x9a, 0x6f, 0x9c, 0xfd, 0x66, 0xc4, 0xae, 0x60, 0xab, 0xae, 0x7d, 0x6e, 0x41,
	0x78, 0x6b, 0xbb, 0x7e, 0xea, 0x8a, 0xad, 0xfb, 0xf0, 0xec, 0x43, 0xe7, 0x4a, 0xe7, 0x67, 0xc2,
	0x99, 0x60, 0x03, 0x9a, 0xdd, 0x2b, 0x9d, 0x8f, 0x94, 0xc2, 0xd2, 0xff, 0x2a, 0xa6, 0xc9, 0xbc,
	0xa4, 0x2b, 0x71, 0x9e, 0x25, 0xac, 0x34, 0xa2, 0x86, 0x90, 0x0a, 0x13, 0x3d, 0x4d, 0x34, 0x1a,
	0x47, 0xda, 0xf1, 0x02, 0xa8, 0x5b, 0x1b, 0x36, 0xad, 0xfd, 0x17, 0x82, 0x09, 0xce, 0x39, 0x4a,
	0x27, 0xdd, 0x45, 0x4d, 0xcf, 0x60, 0xa3, 0x67, 0xfd, 0x25, 0xcf, 0x2e, 0x03, 0xf3, 0xb7, 0xec,
	0xf1, 0x8f, 0x00, 0x00, 0x00, 0xff, 0xff, 0x0a, 0x5b, 0xc8, 0xda, 0xb3, 0x09, 0x00, 0x00,
}
